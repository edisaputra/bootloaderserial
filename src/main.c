#include "stm32f10x.h"
#include "LL_usart.h"
#include "string.h"

typedef void (*vector_table_entry_t)(void);

typedef struct {
	unsigned int *initial_sp_value; /**< Initial stack pointer value. */
	vector_table_entry_t reset;
} vector_table_t;



uint8_t checkUserCode(u32 usrAddr) {
    u32 sp = *(vu32 *) usrAddr;

    if ((sp & 0x2FFE0000) == 0x20000000) {
        return 1;
    } else {
        return 0;
    }
}

#define NVIC_VECTRESET              0         /*!< Vector Reset Bit             */
#define NVIC_SYSRESETREQ            2         /*!< System Reset Request         */
#define NVIC_AIRCR_VECTKEY    (0x5FA << 16)   /*!< AIRCR Key for write access   */
#define NVIC_AIRCR_ENDIANESS        15

void setMspAndJump(u32 usrAddr) {
	vector_table_t* app_vector_table = (vector_table_t*)usrAddr;

	SCB->VTOR = usrAddr;


	/* Initialize the application's stack pointer */
	__set_MSP((uint32_t)(app_vector_table->initial_sp_value));

	/* Jump to the application entry point */
	app_vector_table->reset();

	while (1);
}

#define SERIAL1 	0

void softReset(){
	uint32_t prigroup = SCB->AIRCR & SCB_AIRCR_PRIGROUP;
	SCB->AIRCR = SCB_AIRCR_VECTKEY | SCB_AIRCR_SYSRESETREQ | prigroup;
	asm volatile("dsb");
	while(1);
}

volatile uint32_t time=0;

void SysTick_Handler(void){
	time++;
}

uint8_t enterBootRequest(){
	uint16_t data1, data2, data3;
	if( (RCC->CSR & (1<<28)) != 0) {
		RCC_ClearFlag();
		data1 = BKP_ReadBackupRegister(BKP_DR1);
		data2 = BKP_ReadBackupRegister(BKP_DR2);
		data3 = BKP_ReadBackupRegister(BKP_DR3);
		if(data1 == 0x00 && data2 == 0xFF && data3 == 0x0A){
			BKP_WriteBackupRegister(BKP_DR1,0);
			BKP_WriteBackupRegister(BKP_DR2,0);
			BKP_WriteBackupRegister(BKP_DR3,0);
			return 1;
		}
	}
	return 0;
}

void delay(uint32_t ms){
	uint32_t c = time + ms;
	while(time<c){

	}
}

int main(){
	uint32_t address = 0x8008000;
	uint8_t addr,i=0;
	uint16_t data;
	FLASH_Status fs;
	uint8_t buffer[25];
	GPIO_InitTypeDef gpio;
	SystemInit();
	initUsart(SERIAL1,9600,0);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	PWR_BackupAccessCmd(ENABLE);
	BKP_ClearFlag();

	SysTick_Config(SystemCoreClock/1000);
//
//	while(usartDataAvailable(0)>0)readUsart(0);
//	gpio.GPIO_Mode = GPIO_Mode_IPU;
//	gpio.GPIO_Pin = GPIO_Pin_0;
//	gpio.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOA,&gpio);
//	GPIO_SetBits(GPIOA,GPIO_Pin_0);
//	time = 0;
//	while(time<1000){
//		if(GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)==0){
//			time = 0;
//			break;
//		}
//	}
//	if(time>100){
//		if(checkUserCode(0x8008000)){
//			setMspAndJump(0x8008000);
//		}
//	}
//	writeUsart(0,0);
//	writeUsart(0,0xFF);
//	writeUsart(0,0x00);
//

	if(!enterBootRequest()){
		if(checkUserCode(0x8008000)){
			deInitUsart(0);
			delay(300);
			setMspAndJump(0x8008000);
		}
	}

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin = GPIO_Pin_13;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC,&gpio);
	while(1){
		if(time>500){
			time = 0;
			GPIOC->ODR ^= (1<<13);
		}
		if(usartDataAvailable(SERIAL1)>0){
			buffer[i] = readUsart(SERIAL1);
			i++;
			if(buffer[0] != 'A'){
				writeUsart(SERIAL1,0xFF);
				memset(buffer,0,sizeof(buffer));
				i=0;
				continue;
			}
			if(i == (buffer[1] + 3)){
				if(buffer[i-1] != 'E'){
					writeUsart(SERIAL1,0xFE);
					memset(buffer,0,sizeof(buffer));
					i = 0;
				}else {
					if(buffer[1] == 0){
						writeUsart(SERIAL1,'E');
						address = time + 100;
						while(address>time);
						NVIC_SystemReset();
//						softReset();
					}
					GPIO_ResetBits(GPIOC,GPIO_Pin_13);
					FLASH_Unlock();
					for(addr = 0; addr<buffer[1];addr+=2){
						if((address)%0x400==0){
							fs = FLASH_ErasePage(address);
							if(fs!=FLASH_COMPLETE){
								i = 0;
								writeUsart(SERIAL1,'C');
								continue;
							}
						}
						data = buffer[addr+2] | (buffer[addr+3] << 8 );
						fs = FLASH_ProgramHalfWord(address,data);
						if(fs!=FLASH_COMPLETE){
							i = 0;
							writeUsart(SERIAL1,'F');
							continue;
						}
						address+=2;
					}
					i = 0;
					memset(buffer,0,sizeof(buffer));
					FLASH_Lock();
					GPIO_SetBits(GPIOC,GPIO_Pin_13);
					writeUsart(SERIAL1,'B');
				}
			}
		}
	}
}
