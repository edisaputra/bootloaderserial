/*
 * LL_usart.h
 *
 *  Created on: 5 Nov 2018
 *      Author: NERD
 */

#ifndef LL_LIB_INC_LL_USART_H_
#define LL_LIB_INC_LL_USART_H_

#include "stdint.h"

#define USART_MAX_BUFFER	128
#define MAX_USART			3

#if defined (__cplusplus)
extern "C" {
#endif

void initUsart(uint8_t dev, uint32_t baudrate, uint8_t config);

void deInitUsart(uint8_t dev);

uint8_t writeUsart(uint8_t dev, uint8_t data);

uint8_t usartDataAvailable(uint8_t);

uint8_t readUsart(uint8_t);

#if defined (__cplusplus)
}
#endif

#endif /* LL_LIB_INC_LL_USART_H_ */
